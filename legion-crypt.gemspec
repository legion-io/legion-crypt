# frozen_string_literal: true

require_relative 'lib/legion/crypt/version'

Gem::Specification.new do |spec|
  spec.name          = 'legion-crypt'
  spec.version       = Legion::Crypt::VERSION
  spec.authors       = ['Esity']
  spec.email         = ['matthewdiverson@gmail.com']

  spec.summary       = 'Legion::Vault is used to keep things safe'
  spec.description   = 'Integrates with Hashicorps vault and other encryption type things'
  spec.homepage      = 'https://bitbucket.org/legion-io/legion-vault/'
  spec.license       = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.5.0')

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://bitbucket.org/legion-io/legion/'
  spec.metadata['changelog_uri'] = 'https://bitbucket.org/legion-io/legion/src/master/CHANGELOG.md'
  spec.metadata['wiki_uri'] = 'https://bitbucket.org/legion-io/legion-crypt/wiki'
  spec.metadata['bug_tracker_uri'] = 'https://bitbucket.org/legion-io/legion-crypt/issues'

  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.require_paths = ['lib']

  spec.add_dependency 'vault', '>= 0.15.0'

  spec.add_development_dependency 'legionio'
  spec.add_development_dependency 'legion-logging'
  spec.add_development_dependency 'legion-settings'
  spec.add_development_dependency 'legion-transport'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rspec_junit_formatter'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov', '< 0.18.0'
  spec.add_development_dependency 'simplecov_json_formatter'
end
