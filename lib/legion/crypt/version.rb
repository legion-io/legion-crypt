# frozen_string_literal: true

module Legion
  module Crypt
    VERSION = '0.3.0'
  end
end
